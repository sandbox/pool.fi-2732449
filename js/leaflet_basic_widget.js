(function ($) {
  $(document).bind('leaflet.map', function(e, map, lMap) {
    var moduleSettings = lMap.options.leafletBasicWidget || false;
    if (moduleSettings) {
      var lMarker = false;
      var zoom = map.settings.zoom ? map.settings.zoom : map.settings.zoomDefault;
      var $valueField = $('#' + moduleSettings.id);
      var createMarker = function(latLng) {
        lMarker = new L.Marker(latLng, {draggable: true});

        // Update field value when marker is moved.
        lMarker.on('move', function(e) {
          var value = JSON.stringify(this.toGeoJSON().geometry)
          $valueField.val(value)
        });
        // Remove marker on right-click.
        lMarker.on('contextmenu', function(e) {
          lMap.removeLayer(this);
          $valueField.val('')
        });
        
        // Trigger move to set default value.
        lMarker.fire('move');
        return lMarker;
      }

      // Create new marker on empty map click.
      lMap.on('click', function(e) {
        if (!lMarker) {
          lMarker = createMarker(e.latlng).addTo(lMap);
        }
        else if (!lMarker._map){
          lMarker.setLatLng(e.latlng).addTo(lMap);
        }
      });
      
      // Add marker if value exists.
      currentValue = $valueField.val()
      if (currentValue) {
        var coords = JSON.parse(currentValue).coordinates;
        var latLng = new L.LatLng(coords[1], coords[0]).wrap();
        createMarker(latLng).addTo(lMap);
        lMap.setView(latLng, zoom);
      }
      else {
        lMap.setView(new L.LatLng(moduleSettings.center.lat, moduleSettings.center.lon), zoom);
      }
    }
  });
}(jQuery));
